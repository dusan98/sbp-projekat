//1.Verzija-bez koriscenja sablona
//1.Koji district ima najveći udeo prekršaja gde je osoba vozila pod dejstvom alkohola. Iz kojih gradova je najveći broj vozača u ovim prekršajima. 
/*
db.violations_no_patterns.aggregate([
{$group: {_id: '$district', 
           num_of_violations:{$sum:1},
           cities: {"$push": {
            "$cond":[
                {'$eq': ['$violations.alcohol', 'Yes']},
                '$violations.driver_city',
                'NOWHERE'
            ]}},
           'with_alcohol': { 
                '$sum': {
                    '$cond': [
                        { '$eq': ['$violations.alcohol', 'Yes']}, 
                        1, 
                        0
                    ]
                }
            }
           }
           
},
{ $project: {
        "_id":"$_id",
        "percent_with_alcohol":{$multiply: [{$divide: ['$with_alcohol','$num_of_violations']},100]},
        cities:'$cities'
        }
},
{$sort: {percent_with_alcohol:-1}},    
{$limit:1},
{$unwind: '$cities'},
{$match: { cities:{$ne:'NOWHERE' }}},
{$group: {_id:'$cities','num_of_drunk_drivers':{$sum:1},'district':{$first:'$_id'}}},
{$sort:{num_of_drunk_drivers:-1}},
{$limit:3}
])
*/

//2.Koji procenat ljudi je dobio kaznu a koji upozorenje, grupisati po rasi i polu.
/*
db.violations_no_patterns.aggregate([
{$group:{
    _id:{gender:'$violations.gender',race:'$violations.race'},
    'citations':{ 
                '$sum': {
                    '$cond': [
                        { '$eq': ['$violations.violation_type', 'Citation']}, 
                        1, 
                        0
                    ]
                }
            },
      'warnings': { 
                '$sum': {
                    '$cond': [
                        { '$eq': ['$violations.violation_type', 'Warning']}, 
                        1, 
                        0
                    ]
                }
            },
       'SEROs': { 
                '$sum': {
                    '$cond': [
                        { '$eq': ['$violations.violation_type', 'SERO']}, 
                        1, 
                        0
                    ]
                }
            },
        'ESEROs': { 
                '$sum': {
                    '$cond': [
                        { '$eq': ['$violations.violation_type', 'ESERO']}, 
                        1, 
                        0
                    ]
                }
            },
      'total_violations':{$sum:1}
    }}
,
{$match: {$and:
[
    {'_id.race':{$nin:['OTHER']}},
    {'_id.gender':{$in:['M','F']}}
    ]}}
,
{$addFields: {
       percent_of_citations: {$divide: ['$citations','$total_violations'] } ,
       percent_of_warnings: {$divide:['$warnings','$total_violations']}
     }},
{$sort:{
    percent_of_citations:-1
}}  
])

*/

//3.Koji proizvođači automobila su najviše i najmanje zastupljeni u prekršajima u kojima je osoba dobila naredjenje za popravku elektronike(ESERO). Koja je prosečna starost automobila u ovim prekršajima.
/*
db.violations_no_patterns.aggregate([
    {$match:
        {
          'violations.violation_type':'ESERO'
         }
     },
     {$group:{
         _id:'$violations.brand',
         total_violations: {$sum:1},
         avg_age:{$avg: { $subtract: [ '$year',"$violations.produc_year"] }}
         }},
      {$sort:{total_violations:-1}},
      {$limit:10},
      {$group: {
            _id: null,
            first: { $first: "$$ROOT" },
            last: { $last: "$$ROOT" }
}}
])
*/
/* POMOCNI ZA FILTRIRANJE
db.violations_no_patterns.aggregate([
    {$group:{_id:'$violations.charge_code',sum:{$sum:1}}},
    {$match: { $or: [{_id: /^13-409/}, {_id: /^21-801.1/}]}},
    {$sort:{'sum':-1}}
])*/



//4.Koja rasa ima najveći udeo prekršaja u kojima nemaju tražene dokumente ili su prekršili ograničenje brzine.
/*
db.violations_no_patterns.aggregate([
    {$group:{
        _id: '$violations.race',
        total: {$sum:1},
        docsOrSpeed: { 
                $sum: {
                    $cond: [
                     { $or: 
                         [{$regexMatch: { input: "$violations.charge_code", regex: /^13-409/ }},
                          {$regexMatch: { input: "$violations.charge_code", regex: /^21-801.1/}}]
                     }, 
                        1, 
                        0]
                }
            }         
        }
        },
        {$addFields: {
        percent_of_docs: {$divide: ['$docsOrSpeed','$total'] } ,
        }},
        {$sort:{percent_of_docs:-1}},
        {$limit:2}
])*/
/* POMOCNA geoWithin

db.violations_no_patterns.aggregate([
{$match:
    { 'violations.location': { $geoWithin: { $center: [ [-74, 40.74], 10 ] } }}
}]
)*/




//5.Po godinama i kvartalima koja rasa ima najveci broj prekrsaja. Koje su prosecne koordinate za njihove prekrsaje?
/*

db.violations_no_patterns.aggregate([

                { $match : 

                    {

                    "year" : {$ne: "0"} , 

                    "month": {$ne:'0'}, 

                    "violations.location.coordinates":  {$nin: [NaN,null]},

                    "violations.location.coordinates":  {$nin: [NaN,null]}}},

                { "$unwind": "$violations.location.coordinates" },

                { "$group": { 

                                "_id": "$_id",

                                "year": {$first:'$year'},

                                'month': {$first:'$month'},

                                'race': {$first:'$violations.race'},

                                "lat": { "$first": "$violations.location.coordinates" },

                                "lon": { "$last": "$violations.location.coordinates" }

                       }},

                { $group : {

                    "_id": {

                        'year':'$year',

                        "race":'$race',

                        "quarter" : {

                            $cond: [{ $in: ["$month", [10,11,12] ]}, 4, 

                            {

                                $cond: [{ $in: ["$month", [7,8,9] ]}, 3,

                                {

                                    $cond: [{ $in: ["$month", [4,5,6] ]}, 2,

                                    {

                                        $cond: [{ $in: ["$month", [1,2,3] ]}, 1, 0]

                                    }]

                                }]

                            }]}

                        },

                    "average_lon": {$avg:'$lon'},

                    "average_lat": {$avg:'$lat'},

                    "total_violations": {$sum : 1}

                }}

               ,{$sort: {total_violations : -1}},

                { $group : {

                    "_id": {

                        "year": "$_id.year",

                        "quarter" : "$_id.quarter"

                        },

                    "race_with_most_violations":{$first:'$_id.race'},

                    "average_lon": {$first:'$average_lon'},

                    "average_lat": {$first:'$average_lat'},

                    "total_violations": {$first : "$total_violations"}

                }},{

                    $sort: {'_id.year':1,'_id.quarter':1}

                    

                    

                    }

                ],

                { allowDiskUse: true }

)*/


//2.Verzija izvrseno grupisanje po godini,mesecu,districtu
//1.Koji district ima najveći udeo prekršaja gde je osoba vozila pod dejstvom alkohola. Iz kojih gradova je najveći broj vozača u ovim prekršajima.
/*
db.violations_with_groups.aggregate([

{$unwind:'$violations'},

{$group: {_id: '$district', 

           num_of_violations:{$sum:1},

           cities: {"$push": {

            "$cond":[

                {'$eq': ['$violations.alcohol', 'Yes']},

                '$violations.driver_city',

                'NOWHERE'

            ]}},

           'with_alcohol': { 

                '$sum': {

                    '$cond': [

                        { '$eq': ['$violations.alcohol', 'Yes']}, 

                        1, 

                        0

                    ]

                }

            }

           }       

},

{ $project: {

        "_id":"$_id",

        "percent_with_alcohol":{$multiply: [{$divide: ['$with_alcohol','$num_of_violations']},100]},

        cities:'$cities'

        }

},

{$sort: {percent_with_alcohol:-1}},    

{$limit:1},

{$unwind: '$cities'},

{$match: { cities:{$ne:'NOWHERE' }}},

{$group: {_id:'$cities','num_of_drunk_drivers':{$sum:1},'district':{$first:'$_id'}}},

{$sort:{num_of_drunk_drivers:-1}},

{$limit:3}

])

*/

//2.Koji procenat ljudi je dobio kaznu a koji upozorenje, grupisati po rasi i polu.
/*
db.violations_with_groups.aggregate([

{$unwind:'$violations'},

{$group:{

    _id:{gender:'$violations.gender',race:'$violations.race'},

    'citations':{ 

                '$sum': {

                    '$cond': [

                        { '$eq': ['$violations.violation_type', 'Citation']}, 

                        1, 

                        0

                    ]

                }

            },

      'warnings': { 

                '$sum': {

                    '$cond': [

                        { '$eq': ['$violations.violation_type', 'Warning']}, 

                        1, 

                        0

                    ]

                }

            },

       'SEROs': { 

                '$sum': {

                    '$cond': [

                        { '$eq': ['$violations.violation_type', 'SERO']}, 

                        1, 

                        0

                    ]

                }

            },

        'ESEROs': { 

                '$sum': {

                    '$cond': [

                        { '$eq': ['$violations.violation_type', 'ESERO']}, 

                        1, 

                        0

                    ]

                }

            },

      'total_violations':{$sum:1}

    }}

,

{$match: {$and:

[

    {'_id.race':{$nin:['OTHER']}},

    {'_id.gender':{$in:['M','F']}}

    ]}}

,

{$addFields: {

       percent_of_citations: {$divide: ['$citations','$total_violations'] } ,

       percent_of_warnings: {$divide:['$warnings','$total_violations']}

     }},

{$sort:{

    percent_of_citations:-1

}}  

])*/


//3.Koji proizvođači automobila su najviše i najmanje zastupljeni u prekršajima u kojima je osoba dobila naredjenje za popravku elektronike(ESERO). 
/* 
db.violations_with_groups.aggregate([

{$unwind:'$violations'},

    {$match:

        {

          'violations.violation_type':'ESERO'

         }

     },

     {$group:{

         _id:'$violations.brand',

         total_violations: {$sum:1},

         avg_age:{$avg: { $subtract: [ '$year',"$violations.produc_year"] }}

         }},

      {$sort:{total_violations:-1}},

      {$limit:10},

      {$group: {

            _id: null,

            first: { $first: "$$ROOT" },

            last: { $last: "$$ROOT" }

}}

])*/


//4.Koja rasa ima najveći udeo prekršaja u kojima nemaju tražene dokumente ili su prekršili ograničenje brzine.
/*db.violations_with_groups.aggregate([

    {$unwind:'$violations'},

    {$group:{

        _id: '$violations.race',

        total: {$sum:1},

        docsOrSpeed: { 

                $sum: {

                    $cond: [

                     { $or: 

                         [{$regexMatch: { input: "$violations.charge_code", regex: /^13-409/ }},

                          {$regexMatch: { input: "$violations.charge_code", regex: /^21-801.1/}}]

                     }, 

                        1, 

                        0]

                }

            }         

        }

        },

        {$addFields: {

        percent_of_docs: {$divide: ['$docsOrSpeed','$total'] } ,

        }},

        {$sort:{percent_of_docs:-1}},

        {$limit:2}

])*/


//5.Po godinama i kvartalima koja rasa ima najveci broj prekrsaja. Koje su prosecne koordinate za njihove prekrsaje?
/*

db.violations_with_groups.aggregate([

                {$unwind: '$violations'},

                { $match : 

                    {

                    "year" : {$ne: "0"} , 

                    "month": {$ne:'0'}, 

                    "violations.location.coordinates":  {$nin: [NaN,null]},

                    "violations.location.coordinates":  {$nin: [NaN,null]}}},

                { "$unwind": "$violations.location.coordinates" },

                { "$group": { 

                                "_id": "$_id",

                                "year": {$first:'$year'},

                                'month': {$first:'$month'},

                                'race': {$first:'$violations.race'},

                                "lat": { "$first": "$violations.location.coordinates" },

                                "lon": { "$last": "$violations.location.coordinates" }

                       }},

                { $group : {

                    "_id": {

                        'year':'$year',

                        "race":'$race',

                        "quarter" : {

                            $cond: [{ $in: ["$month", [10,11,12] ]}, 4, 

                            {

                                $cond: [{ $in: ["$month", [7,8,9] ]}, 3,

                                {

                                    $cond: [{ $in: ["$month", [4,5,6] ]}, 2,

                                    {

                                        $cond: [{ $in: ["$month", [1,2,3] ]}, 1, 0]

                                    }]

                                }]

                            }]}

                        },

                    "average_lon": {$avg:'$lon'},

                    "average_lat": {$avg:'$lat'},

                    "total_violations": {$sum : 1}

                }}

               ,{$sort: {total_violations : -1}},

                { $group : {

                    "_id": {

                        "year": "$_id.year",

                        "quarter" : "$_id.quarter"

                        },

                    "race_with_most_violations":{$first:'$_id.race'},

                    "average_lon": {$first:'$average_lon'},

                    "average_lat": {$first:'$average_lat'},

                    "total_violations": {$first : "$total_violations"}

                }},{

                    $sort: {'_id.year':1,'_id.quarter':1}

                    

                    

                    }

                ],

                { allowDiskUse: true }

)*/





/*

db.violations_no_patterns.createIndex({'year':1})

db.violations_no_patterns.createIndex({'month':1})

db.violations_no_patterns.createIndex({'district':1})

db.violations_no_patterns.createIndex({'state_abv':1})

db.violations_no_patterns.createIndex({'violations.alcohol':1})

db.violations_no_patterns.createIndex({'violations.charge_code':'text'})

//db.violations_no_patterns.createIndex({'violations.location':"2dsphere"})*/
