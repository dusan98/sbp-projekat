import pandas as pd
import pymongo
import os
import math

'''
1.  Koji district ima najveći udeo prekršaja gde je postojao smrtni slučaj. Iz kojih gradova je najveći broj vozača u ovim prekršajima. 
2.	Koji procenat ljudi je dobio kaznu a koji upozorenje, grupisati po rasi i polu.
3.	Koji proizvođači automobila su najviše i najmanje zastupljeni u prekršajima u kojima je osoba vozila pod dejstvom alkohola. Koja je prosečna starost automobila u ovim prekršajima.
4.  Koja rasa ima najveći udeo prekršaja u kojima nemaju tražene dokumente.
5.  Ko češće vrši različite tipove prekršaja. Vozači iz iste države ili iz susednih.
'''

client = pymongo.MongoClient()
db = client['SBP_Project']
violation_collection = db['violations_no_patterns']

# violation_type- Citation,Warning,ESERO-naredjenje za popravku
data_for_write = list()
data = pd.read_csv('traffic_violations.csv')
data.columns = ['date_of_stop', 'time_of_stop', 'agency', 'subagency', 'description', 'street', 'latitude', 'longitude', 'had_accident', 'belt_missing', 'personal_injury', 'property_damage', 'fatal',
                'com_licence', 'hazmat', 'com_vehicle', 'alcohol_present', 'in_work_zone', 'state_abv', 'vehicle_type', 'production_year', 'brand', 'model', 'color', 'violation_type', 'charge_legal_code', 'article', 'caused_accident', 'race', 'gender', 'driver_city',
                'driver_state_abr', 'DL_state_abv', 'arrest_type', 'geolocation']
data['production_year'] = data['production_year'].fillna(0).astype(int)
i = 0
for index, row in data.iterrows():

    # if(i>500):
    # break
    i = i+1
    if(i % 100000 == 0):
        print('\n Analizirano: '+str(i)+" torki "+row['date_of_stop'])
        #print(row['date_of_stop'],row['latitude'], row['longitude'], row['race'],row['geolocation'])

    pom = row['date_of_stop'].split('/')
    month, day, year = int(pom[0]), int(pom[1]), int(pom[2])
    pom = row['time_of_stop'].split(':')
    hour, minute = int(pom[0]), int(pom[1])

    data_for_write.append({
        # 'date':row['date_of_stop'],
        'year': year,
        'month': month,
        # 'day_of_month':day,
        # 'time':row['time_of_stop'],
        'hour': hour,
        'state_abv': row['state_abv'],
        # 'minute':minute,
        'agency': row['agency'],
        'district': row['subagency'],
        'violations': {
            'desc': row['description'],
            'street': row['street'],
            # 'lat':row['latitude'],
            # 'long':row['longitude'],
            'accident': row['had_accident'],
            'belt': row['belt_missing'],
            'per_injury': row['personal_injury'],
            'prop_damage': row['property_damage'],
            'fatal': row['fatal'],
            'com_licence': row['com_licence'],
            'hazmat': row['hazmat'],
            'com_veh': row['com_vehicle'],
            'alcohol': row['alcohol_present'],
            'work_zone': row['in_work_zone'],
            # 'state_abv':row['state_abv'],
            'veh_type': row['vehicle_type'],
            'produc_year': row['production_year'],
            'brand': row['brand'],
            'model': row['model'],
            'color': row['color'],
            'violation_type': row['violation_type'],
            'charge_code': row['charge_legal_code'],
            'article': row['article'],
            'caused_accident': row['caused_accident'],
            'race': row['race'],
            'gender': row['gender'],
            'driver_city': row['driver_city'],
            'driver_state_abv': row['driver_state_abr'],
            'DL_state_abv': row['DL_state_abv'],
            'arrest_type': row['arrest_type'],
            # Geolokoacija-Potrebna za Mongo Geo index i agregacije sa odstojanjem
            'location': {
                'type': 'Point',
                        'coordinates': [row['latitude'], row['longitude']]
            }
        }
        # 'geolocation':row['geolocation']
    })

# for el in data_for_write:
#    print('\n -----------------------------------------------\n ',el)
# violation_collection.insert_many(data_for_write)
print("Total number of rows inserted into database" + str(i))
