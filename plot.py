import numpy as np
import matplotlib.pyplot as plt
data = [
    [2.91, 3.65, 0.86, 3.2, 11.9],
    [2.15, 3.2, 2.67, 2.75, 7.4],
    [3.06, 3.59, 0.905, 3.4, 13.3]
]
X = np.arange(5)
fig, ax = plt.subplots()
v1_bar = ax.bar(X, data[0], color='b', width=0.25)
v2_bar = ax.bar(X + 0.25, data[1], color='g', width=0.25)
v3_bar = ax.bar(X + 0.50, data[2], color='r', width=0.25)

ax.set_ylabel('Time in seconds')
ax.set_xlabel('Num of query')
ax.set_xticks(X + 0.50 / 2)
ax.set_xticklabels(('1.', '2.', '3.', '4.', '5.'))

ax.legend((v1_bar[0], v2_bar[0], v3_bar[0]),
          ('CSV->JSON', 'Group by time,district', 'With index'))
plt.show()
